#!/usr/bin/python
# -*- coding: UTF-8 -*-

import re
import time

from carrega import carrega_confs, carrega_periods
from cfp import get_info

metadados1 = "Title: "
metadados2 = '''
Author: Andrew Toshiaki Nakayama Kurauchi
Summary: Qualis da Capes
Category: Outros
Tags: Qualis, Capes
Date: ''' + time.strftime('%Y-%m-%d') + '\n\n'

javascriptECss = '''<style type="text/css" title="currentStyle">
@import "./css/demo_page.css";
@import "./css/demo_table.css";
</style>
<script type="text/javascript" language="javascript" src="./js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="./js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
'''

javascriptConf = '''
$('#dyntable').dataTable({
"aoColumns": [
{"bSortable": true},
{"bSortable": true},
{"bSortable": true},
{"bSortable": true},
{"iDataSort": 7},
{"bSortable": true},
{"iDataSort": 8},
{"bVisible": false},
{"bVisible": false}
]
});
} );
</script>
'''

javascriptPer = '''
$('#dyntable').dataTable();
} );
</script>
'''

title = '''
<include file="'''

title_end = '''"/>

'''

tableHeader = '''
<table cellpadding="0" cellspacing="0" border="0" class="display" id="dyntable" width="100%">
'''

footer = '''</table>
'''

def makeTHead(heads):
	tHead = '''				<thead>
					<tr>
'''
	for head in heads:
		tHead += '\t\t\t\t\t<th>' + head + '</th>\n'
	tHead += '''					</tr>
				</thead>'''
	return tHead

def makeTBody(elems, campos):
	tBody = '\t\t\t\t<tbody>\n'
	for i in range(len(elems)):
		if i % 2 == 0:
			color = 'even'
		else:
			color = 'odd'
		tBody += '\t\t\t\t\t<tr class="' + color + ' gradeA">\n'
		for campo in campos:
			tBody += '\t\t\t\t\t\t<td>' + elems[i][campo] + '</td>\n'
		tBody += '\t\t\t\t\t</tr>\n'
	tBody += '\t\t\t\t</tbody>\n'
	return tBody

def gera_html_period():
    markdownFilePath = 'out/qualisPeriodico.md'
    includeFilename = 'qualisPeriod.html'
    includeFilePath = 'out/' + includeFilename
    periods = carrega_periods()
    markdown = metadados1 + "Qualis Periódicos" + metadados2
    markdown += javascriptECss + javascriptPer + title + includeFilename + title_end
    include = tableHeader
    include += makeTHead(['ISSN', 'Título', 'Estrato'])
    include += makeTBody(periods, ['issn', 'titulo', 'estrato'])
    include += footer

    f = open(markdownFilePath, 'w')
    f.write(markdown)
    f.close()
    f = open(includeFilePath, 'w')
    f.write(include)
    f.close()
	
def gera_html_conf():
    markdownFilePath = 'out/qualisConferencia.md'
    includeFilename = 'qualisConf.html'
    includeFilePath = 'out/' + includeFilename
    confs = carrega_confs()
    for conf in confs:
    	print "Processando " + conf['sigla'] + "..."
    	when, where, deadline, when_sort, deadline_sort = get_info(conf['sigla'])
    	conf['data'] = when
    	conf['local'] = where
    	conf['data_limite'] = deadline
    	conf['data_sort'] = when_sort
    	conf['data_limite_sort'] = deadline_sort
    
    markdown = metadados1 + "Qualis Conferencias" + metadados2
    markdown += javascriptECss + javascriptConf + title + includeFilename + title_end
    include = tableHeader
    include += makeTHead(['Sigla', 'Nome', 'Índice H', 'Estrato', 'Data', 'Local', 'Deadline'])
    campos = ['sigla', 'nome', 'indice_h', 'estrato', 'data', 'local', 'data_limite', 'data_sort', 'data_limite_sort']
    include += makeTBody(confs, campos)
    include += footer
    
    f = open(markdownFilePath, 'w')
    f.write(markdown)
    f.close()
    f = open(includeFilePath, 'w')
    f.write(include)
    f.close()
	
if __name__=='__main__':
    gera_html_period()
    gera_html_conf()
