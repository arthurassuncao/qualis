// ==UserScript==
// @match http://qualis.capes.gov.br/webqualis/publico/pesquisaPublicaClassificacao.seam
// ==/UserScript==

// 1) Arraste este arquivo para chrome://extensions no Chrome
// 2) Acesse http://qualis.capes.gov.br/webqualis/publico/pesquisaPublicaClassificacao.seam?conversationPropagation=begin
// 3) Filtre por Classificacao / Area de Avaliacao
// 4) Para cada pagina clique no botao "copiar" embaixo de Exportar PDF
// 5) Copie os dados e cole em um arquivo txt
// TODO: Encontrar um jeito melhor de conseguir esses dados

var buttonParent = document.getElementById("consultaPublicaClassificacaoForm:resultadoPesquisaPublicaTituloPorArea");
var button = document.createElement("button");
var node = document.createTextNode("Copiar");
button.appendChild(node);
buttonParent.appendChild(button);

button.onclick = function() {
    text = '';

    corpo = document.getElementById("consultaPublicaClassificacaoForm:listaVeiculos:tb");
    for (var i = 0, linha; linha = corpo.children[i]; i++) {
        for (var j = 0, item; item = linha.children[j]; j++) {
            text += item.innerText + '\n';
        }
    }

    window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
};