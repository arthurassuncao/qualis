Title: Qualis Periódicos
Author: Andrew Toshiaki Nakayama Kurauchi
Summary: Qualis da Capes
Category: Outros
Tags: Qualis, Capes
Date: 2015-01-28

<style type="text/css" title="currentStyle">
@import "../css/demo_page.css";
@import "../css/demo_table.css";
</style>
<script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {

$('#dyntable').dataTable();
} );
</script>

<include file="qualisPeriod.html"/>

